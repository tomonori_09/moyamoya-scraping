import time
import os
import requests
import fire
from bs4 import BeautifulSoup
from url_list import item_list
from janome.tokenizer import Tokenizer

def count_keyword(keyword):
    count_list = []
    for url in item_list:
        soup = parse_html(url)
        title = get_title(soup)
        num_of_app = search_keyword(soup, keyword=keyword)

        count_list.append({'title': title, 'url': url, 'count': num_of_app})

    count_sorted = sorted(count_list, key=lambda x:x['count'], reverse=True)

    with open('output/' + keyword + '.txt', mode='w') as f:
        f.write('キーワード:' + keyword + "\n")
        for item in count_sorted:
            if item['count'] == 0:
                continue
            print("{};{};{}".format(item['title'], item['count'], item['url']))
            f.write("{};{};{}\n".format(item['title'], item['count'], item['url']))


def janome():
    words  = {}
    for url in item_list:
        t = Tokenizer(wakati=True)
        soup = parse_html(url)
        post_text = get_post_text(soup)
        tokens = t.tokenize(post_text, wakati=True)

        for t in tokens:
            if t not in words.keys():
                words[t] = 0

            words[t] += 1

    sorted_words = sorted(words.items(), key=lambda x:x[1], reverse=True)
    print('word;count;length')
    for word in sorted_words:
        if len(word[0]) == 1 or word[1] == 1:
            continue
        print("{};{};{}".format(word[0], word[1], len(word[0])))


def parse_html(url: str):
    filename = url.replace('http://', '').replace('/', '_') + '.html'
    path_name = 'data/' + filename

    # 最初にdownloadしてアクセスを減らす
    if not os.path.exists(path_name):
        r = requests.get(url, timeout=5)
        with open(path_name, 'w') as file:
            file.write(r.text)

        time.sleep(1)
        print('data not found. get ' + url)

    with open(path_name, 'r') as f:
        text = f.read()

    return BeautifulSoup(text, 'html.parser')


def get_title(soup)-> str:
    elements = soup.find('h1', class_="entry-title")
    return elements.get_text().strip()


def get_links(soup):
    links = soup.find_all('a')

    for link in links:
        print(link)


def get_post_text(soup):
    post_text = ''
    elements = soup.find_all('div', class_="textwidget")
    if len(elements) >= 2:
        for p in elements[1].find_all('p'):
            post_text += p.text
    return post_text


def search_keyword(soup, keyword):
    post_text = get_post_text(soup)

    return post_text.count(keyword)


if __name__ == '__main__':
  fire.Fire({
      'count_keyword': count_keyword,
      'janome': janome,
  })
