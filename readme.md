# 概要
[晶文社scrapbook モヤモヤの日々](http://s-scrap.com/4327) キーワード登場数カウント  
2021/05/04 GWモヤモヤな日々○選 記事書くのに作った 

引数で指定したキーワードの出現回数を調べ、回数の多い順にsortする

初回実行時、取得したhtmlを `src/data` へ出力。次回以降はサイトへのアクセスを行わない。  
結果は標準出力と　`src/output` へ出力される

# Usage

```bash
docker compose up -d
docker exec -it moyamoya-app /bin/bash
python job.py count_keyword 双子のライオン堂

第72回　双子のライオン堂の竹田さん;2;http://s-scrap.com/5010
第84回　ダンゴムシを見つける達人;1;http://s-scrap.com/5130
```
